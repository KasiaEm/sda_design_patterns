package com.sda.builder;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        PersonBuilder personBuilder = new PersonBuilder("Zoe", "King", Person.Nationality.UK, LocalDate.now(), Person.Sex.FEMALE);
        Person person = personBuilder
                .employed(false)
                .insured(true)
                .eyeColor(Person.EyeColor.GREEN)
                .homeOwner(false)
                .createPerson();
        System.out.println(person.toString());

    }

}
