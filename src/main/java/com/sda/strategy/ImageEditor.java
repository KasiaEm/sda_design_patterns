package com.sda.strategy;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Stack;

public class ImageEditor {
    private BufferedImage imageTemp;
    private Stack<BufferedImage> operationsDone;

    public void loadImage(String filename){
        try {
            if(filename ==null || filename.isEmpty()){
                System.out.println("Filename is empty.");
            } else {
                imageTemp = ImageIO.read(new File(getClass().getClassLoader().getResource(filename).getFile()));
                System.out.println("Successfuly loaded image " + filename);
            }
            operationsDone = new Stack<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveImage(){
        try {
            if(imageTemp==null){
                System.out.println("There's no image to save.");
            } else {
                ImageIO.write(imageTemp, "jpg", new File("D://tmp.jpg"));
                System.out.println("Successfuly saved image.");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void perform(ImageOperation imageOperation){
        ColorModel cm = imageTemp.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = imageTemp.copyData(null);
        BufferedImage stateBefore = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
        //operation pushed to stack
        operationsDone.push(stateBefore);
        //operation execute
        imageOperation.execute(imageTemp);
    }

    public void undo(){
        if(operationsDone.empty()){
            System.out.println("No operations to undo.");
        } else {
            imageTemp = operationsDone.pop();
            System.out.println("Operation undone.");
        }
    }
}
