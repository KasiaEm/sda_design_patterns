package com.sda.strategy;

import java.awt.*;
import java.awt.image.BufferedImage;

public class DistributeStripes implements ImageOperation {
    private int stripesCount;
    private Color color;

    public DistributeStripes(int stripesCount, Color color) {
        this.stripesCount = stripesCount;
        this.color = color;
    }

    @Override
    public void execute(BufferedImage image) {
        Graphics2D g = image.createGraphics();
        g.setColor(color);
        g.setStroke(new BasicStroke(2f));
        int gap = image.getWidth()/stripesCount;
        for(int i=gap; i<image.getWidth(); i+=gap){
            g.drawLine(i, 0, i, image.getHeight());
        }
        g.dispose();
        System.out.println("DISTRIBUTED STRIPES.");
    }

    @Override
    public String toString() {
        return "DistributeStripes";
    }
}
