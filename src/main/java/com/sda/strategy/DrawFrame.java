package com.sda.strategy;

import java.awt.*;
import java.awt.image.BufferedImage;

public class DrawFrame implements ImageOperation {
    private Color color;

    public DrawFrame(Color color) {
        this.color = color;
    }

    @Override
    public void execute(BufferedImage image) {
        Graphics2D g = image.createGraphics();
        g.setColor(color);
        g.setStroke(new BasicStroke(50f));
        g.drawRect(0, 0, image.getWidth(), image.getHeight());
        g.dispose();
        System.out.println("DREW FRAME.");
    }

    @Override
    public String toString() {
        return "DrawFrame";
    }
}
