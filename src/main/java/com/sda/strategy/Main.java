package com.sda.strategy;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        ImageEditor ie = new ImageEditor();
        ie.loadImage("myImg.jpg");
        ie.perform(new DrawFrame(Color.WHITE));
        /*ie.undo();*/
        ie.perform(new DistributeStripes(20, new Color(197,179,200)));
        ie.perform(new AddCenteredText(new Color(111, 67, 119), "Software Development Academy"));
        ie.saveImage();
    }
}
