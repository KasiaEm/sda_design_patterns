package com.sda.strategy;


import java.awt.*;
import java.awt.image.BufferedImage;

public class AddCenteredText implements ImageOperation {
    private Color color;
    private String text;

    public AddCenteredText(Color color, String text) {
        this.color = color;
        this.text = text;
    }

    @Override
    public void execute(BufferedImage image) {
        Graphics2D g = image.createGraphics();
        Font font = new Font("TimesRoman", Font.PLAIN, 40);
        g.setFont(font);
        FontMetrics fontMetrics = g.getFontMetrics();
        int stringWidth = fontMetrics.stringWidth(text);
        int stringHeight = fontMetrics.getAscent();
        g.setPaint(color);
        g.drawString(text, (image.getWidth() - stringWidth) / 2, image.getHeight() / 2 + stringHeight / 2);
        g.dispose();
        System.out.println("ADDED TEXT.");
    }

    @Override
    public String toString() {
        return "AddCenteredText";
    }
}
