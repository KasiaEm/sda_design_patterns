package com.sda.strategy;

import java.awt.image.BufferedImage;

public class OperationDone {
    private BufferedImage imageBefore;
    private ImageOperation imageOperation;

    public OperationDone(BufferedImage imageBefore, ImageOperation imageOperation) {
        this.imageBefore = imageBefore;
        this.imageOperation = imageOperation;
    }

    public BufferedImage getImageBefore() {
        return imageBefore;
    }

    public ImageOperation getImageOperation() {
        return imageOperation;
    }
}
