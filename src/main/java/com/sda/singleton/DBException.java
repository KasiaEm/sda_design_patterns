package com.sda.singleton;

public class DBException extends Exception {
    public DBException(String message){
        super(message);
    }
}
