package com.sda.singleton;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class Database {
    private static final Database CONNECTION = new Database();
    private final Set<User> users = new HashSet<>();
    private String loggedIn;

    private Database(){}

    public static Database getConnection(){
        return CONNECTION;
    }

    public void addUser(User user) throws DBException {
        if(users.stream().anyMatch(u -> u.getName().equals(user.getName()))){
            throw new DBException("Username already exists!");
        }
        users.add(user);
    }

    public User login(User user) throws DBException {
        Optional<User> userFromDB = users.stream()
                .filter(u -> u.getName().equals(user.getName()))
                .findFirst();
        userFromDB.filter(u -> u.getPassword().equals(user.getPassword()))
                .orElseThrow(() -> new DBException("Wrong username or password."));
        loggedIn = user.getName();
        /*User userFromDB = null;
        for(User u: users){
            if(u.getName().equals(user.getName())){
                if(u.getPassword().equals(user.getPassword())){
                    loggedIn = u.getName();
                    userFromDB = u;
                    break;
                }
            }
        }
        if(userFromDB == null){
            throw new DBException("Wrong username or password.");
        }*/
        return userFromDB.get();
        /*return userFromDB;*/
    }

    public void logout(){
        loggedIn = null;
    }

    public String getLoggedIn() {
        return loggedIn;
    }
}
