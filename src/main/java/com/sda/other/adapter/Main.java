package com.sda.other.adapter;

public class Main {
    public static void main(String[] args) {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "track.mp3");
        audioPlayer.play("mp4", "movie.mp4");
        audioPlayer.play("vlc", "movie2.vlc");
        audioPlayer.play("avi", "movie3.avi");
    }
}
