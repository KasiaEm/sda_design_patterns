package com.sda.other.adapter;

public interface MediaPlayer {
    public void play(String audioType, String fileName);
}
