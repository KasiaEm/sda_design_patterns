package com.sda.other.command;

public interface Order {
    void execute();
}
