package com.sda.other.common;

public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing rectangle.");
    }
}
