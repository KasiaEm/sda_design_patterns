package com.sda.other.common;

public enum ShapeType {
    CIRCLE, RECTANGLE
}
