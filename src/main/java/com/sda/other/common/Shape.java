package com.sda.other.common;

public interface Shape {
    void draw();
}
