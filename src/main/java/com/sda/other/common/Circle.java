package com.sda.other.common;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Drawing circle.");
    }
}
