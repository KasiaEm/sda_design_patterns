package com.sda.other.prototype;

import java.util.Objects;

public class Attachment implements Cloneable {
    public enum AttachmentType {
        PDF, IMAGE, XML, VIDEO
    }

    public Attachment(String path, String name, AttachmentType attachmentType) {
        this.path = path;
        this.name = name;
        this.attachmentType = attachmentType;
    }

    private String path;
    private String name;
    private AttachmentType attachmentType;

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public AttachmentType getAttachmentType() {
        return attachmentType;
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "path='" + path + '\'' +
                ", name='" + name + '\'' +
                ", attachmentType=" + attachmentType +
                '}';
    }

    public Attachment clone() {
        Attachment clone = null;
        try {
            clone = (Attachment) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attachment that = (Attachment) o;
        return Objects.equals(path, that.path) &&
                Objects.equals(name, that.name) &&
                attachmentType == that.attachmentType;
    }
}
