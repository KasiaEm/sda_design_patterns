package com.sda.other.prototype;

public class Main {
    public static void main(String[] args) {
        FormsPrototypeRegistry r = FormsPrototypeRegistry.getInstance();
        ContractForm f = (ContractForm) r.getTemplate(TemplateType.SDA);
        ContractForm f2 = (ContractForm) r.getTemplate(TemplateType.SDA);
        System.out.println(f.equals(f2));
        System.out.println(f == f2);
        //System.out.println(f);
        //System.out.println(f2);
    }
}
