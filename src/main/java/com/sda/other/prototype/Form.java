package com.sda.other.prototype;

public abstract class Form implements Cloneable {
    public enum FormStyle {
        OFFICIAL, PLAIN, DARK, LIGHT
    }

    private FormStyle formStyle;

    public Form() {
    }

    public Form(FormStyle formStyle) {
        this.formStyle = formStyle;
    }

    public FormStyle getFormStyle() {
        return formStyle;
    }

    public void setFormStyle(FormStyle formStyle) {
        this.formStyle = formStyle;
    }

    @Override
    public String toString() {
        return "Form{" +
                "formStyle=" + formStyle +
                '}';
    }

    public Form clone() {
        Form clone = null;
        try {
            clone = (Form) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Form form = (Form) o;
        return formStyle == form.formStyle;
    }
}
