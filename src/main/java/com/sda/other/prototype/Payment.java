package com.sda.other.prototype;

import java.math.BigDecimal;
import java.util.Objects;

public class Payment implements Cloneable {
    public enum PaymentUnit {
        HOUR, DAY, WEEK, MONTH, CONTRACT_TIME, WORK, ERRAND
    }

    public enum PaymentCurrency {
        PLN, USD, EUR
    }

    private PaymentUnit paymentUnit;
    private PaymentCurrency paymentCurrency;
    private BigDecimal paymentAmount;

    public Payment(PaymentUnit paymentUnit, PaymentCurrency paymentCurrency, BigDecimal paymentAmount) {
        this.paymentUnit = paymentUnit;
        this.paymentCurrency = paymentCurrency;
        this.paymentAmount = paymentAmount;
    }

    public PaymentUnit getPaymentUnit() {
        return paymentUnit;
    }

    public PaymentCurrency getPaymentCurrency() {
        return paymentCurrency;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "paymentUnit=" + paymentUnit +
                ", paymentCurrency=" + paymentCurrency +
                ", paymentAmount=" + paymentAmount +
                '}';
    }

    public Payment clone() {
        Payment clone = null;
        try {
            clone = (Payment) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return paymentUnit == payment.paymentUnit &&
                paymentCurrency == payment.paymentCurrency &&
                Objects.equals(paymentAmount, payment.paymentAmount);
    }
}
