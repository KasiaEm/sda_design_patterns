package com.sda.other.prototype;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FormsPrototypeRegistry {
    private static FormsPrototypeRegistry instance = new FormsPrototypeRegistry();
    private Map<TemplateType, Form> formTemplates;

    private FormsPrototypeRegistry() {
        prepareTemplates();
    }

    private void prepareTemplates() {
        formTemplates = new HashMap<>();
        ContractForm formSDA = new ContractForm(Form.FormStyle.OFFICIAL, "SDA_XXXX_XXXX");
        Subject subjectSDA = new Subject();
        subjectSDA.setFullName("John Doe");
        subjectSDA.setEmail("sda@sda.com");
        subjectSDA.setSubjectId("1000000");
        subjectSDA.setSubjectName("Software Development Agency");
        Address addressSDA = new Address(Address.Country.PL, "slaskie", "Katowice", "40-000", "Wesola 15");
        subjectSDA.setAddress(addressSDA);
        formSDA.setEmployer(subjectSDA);
        List<Attachment> attachmentListSDA = new LinkedList<>();
        attachmentListSDA.add(new Attachment("/SDA", "RODO", Attachment.AttachmentType.PDF));
        formSDA.setAttachments(attachmentListSDA);
        Payment paymentSDA = new Payment(Payment.PaymentUnit.HOUR, Payment.PaymentCurrency.PLN, BigDecimal.valueOf(200));
        formSDA.setPayment(paymentSDA);
        formTemplates.put(TemplateType.SDA, formSDA);
    }

    public Form getTemplate(TemplateType formTemplate) {
        return formTemplates.get(formTemplate).clone();
    }

    public static FormsPrototypeRegistry getInstance() {
        return instance;
    }
}
