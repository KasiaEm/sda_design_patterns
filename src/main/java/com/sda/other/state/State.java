package com.sda.other.state;

public interface State {
    public void doAction(Context context);
}
