package com.sda.other.factory;

import com.sda.other.common.Shape;
import com.sda.other.common.ShapeType;

public class Main {
    public static void main(String[] args) {
        Factory shapeFactory = new Factory();

        //get an object of Circle and call its draw method.
        Shape shape1 = shapeFactory.getShape(ShapeType.CIRCLE);
        shape1.draw();

        //get an object of Rectangle and call its draw method.
        Shape shape2 = shapeFactory.getShape(ShapeType.RECTANGLE);
        shape2.draw();
    }
}
