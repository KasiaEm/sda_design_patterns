package com.sda.other.factory;

import com.sda.other.common.Circle;
import com.sda.other.common.Rectangle;
import com.sda.other.common.Shape;
import com.sda.other.common.ShapeType;

public class Factory {
    public Shape getShape(ShapeType shapeType){
        if(shapeType.equals(ShapeType.CIRCLE)){
            return new Circle();

        } else if(shapeType.equals(ShapeType.RECTANGLE)){
            return new Rectangle();

        }
        return null;
    }
}
