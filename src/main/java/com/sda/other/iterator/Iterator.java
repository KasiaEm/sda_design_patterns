package com.sda.other.iterator;

public interface Iterator {
    public boolean hasNext();
    public Object next();
}
