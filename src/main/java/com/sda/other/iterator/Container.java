package com.sda.other.iterator;

public interface Container {
    public Iterator getIterator();
}
