package com.sda.other.interpreter;

public interface Expression {
    public boolean interpret(String context);
}
