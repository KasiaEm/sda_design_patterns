package com.sda.other.bridge;

public interface DrawAPI {
    public void drawCircle(int radius, int x, int y);
}
