package com.sda.other.facade;

public class Main {
    public static void main(String[] args) {
        Facade shapeMaker = new Facade();

        shapeMaker.drawCircle();
        shapeMaker.drawRectangle();
    }
}
