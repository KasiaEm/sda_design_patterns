package com.sda.other.facade;

import com.sda.other.common.Circle;
import com.sda.other.common.Rectangle;
import com.sda.other.common.Shape;

public class Facade {
    private Shape circle;
    private Shape rectangle;

    public Facade() {
        circle = new Circle();
        rectangle = new Rectangle();
    }

    public void drawCircle(){
        circle.draw();
    }
    public void drawRectangle(){
        rectangle.draw();
    }
}
