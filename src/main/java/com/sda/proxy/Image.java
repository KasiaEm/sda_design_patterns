package com.sda.proxy;

public interface Image {
    void display();
}
