package com.sda.proxy;

public class Main {
    public static void main(String[] args) {
        //image will be loaded from disk
        Image image = new ProxyImage("test.jpg");

        //image will not be loaded from disk
        image.display();
    }
}
