package com.sda.absfactory;

public class CarNrPlate extends NrPlate {
    private boolean onlyFront;
    private boolean sticker;

    public CarNrPlate(Language language, Size size, boolean onlyFront, boolean sticker) {
        super(language, size);
        this.onlyFront = onlyFront;
        this.sticker = sticker;
    }

    @Override
    public void show() {
        System.out.println(getNr() + " " + getSize() + " Only front? " + onlyFront + " Sticker? " + sticker);
    }

    public boolean isOnlyFront() {
        return onlyFront;
    }

    public void setOnlyFront(boolean onlyFront) {
        this.onlyFront = onlyFront;
    }

    public boolean isSticker() {
        return sticker;
    }

    public void setSticker(boolean sticker) {
        this.sticker = sticker;
    }
}
