package com.sda.absfactory;

public enum PlateType {
    CAR, MOTORCYCLE, TRUCK
}
