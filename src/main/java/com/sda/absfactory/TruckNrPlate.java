package com.sda.absfactory;

public class TruckNrPlate extends NrPlate {
    private boolean additionalSpeedInfo;

    public TruckNrPlate(Language language, Size size, boolean additionalSpeedInfo) {
        super(language, size);
        this.additionalSpeedInfo = additionalSpeedInfo;
    }

    @Override
    public void show() {
        System.out.println(getNr() + " " + getSize() + "Additional Info? " + additionalSpeedInfo);
    }

    public boolean isAdditionalSpeedInfo() {
        return additionalSpeedInfo;
    }

    public void setAdditionalSpeedInfo(boolean additionalSpeedInfo) {
        this.additionalSpeedInfo = additionalSpeedInfo;
    }
}
