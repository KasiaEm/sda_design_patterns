package com.sda.absfactory;

public class PolishPlateFactory extends AbstractPlateFactory {
    @Override
    public NrPlate getPlate(PlateType plateType) {
        if(plateType.equals(PlateType.CAR)){
            return new CarNrPlate(Language.PL, Size.POLISH, false, true);
        } else if (plateType.equals(PlateType.MOTORCYCLE)){
            return new MotorcycleNrPlate(Language.PL, Size.POLISH_MOTORCYCLE, null);
        } else if (plateType.equals(PlateType.TRUCK)){
            return new TruckNrPlate(Language.PL, Size.POLISH, true);
        }
        return null;
    }
}
