package com.sda.absfactory;

public abstract class AbstractPlateFactory {
    public abstract NrPlate getPlate(PlateType plateType);
}
