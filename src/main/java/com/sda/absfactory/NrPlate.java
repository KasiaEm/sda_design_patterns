package com.sda.absfactory;

public abstract class NrPlate {
    private String nr;
    private Language language;
    private Size size;

    public NrPlate() {
    }

    public NrPlate(Language language, Size size) {
        this.language = language;
        this.size = size;
    }

    public NrPlate(String nr, Language language, Size size) {
        this.nr = nr;
        this.language = language;
        this.size = size;
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    abstract void show();
}
