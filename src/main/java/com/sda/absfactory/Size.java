package com.sda.absfactory;

public enum Size {
    AMERICAN_CAR, AMERICAN_MOTORCYCLE, AMERICAN_TRUCK, POLISH, POLISH_MOTORCYCLE
}
