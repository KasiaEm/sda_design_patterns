package com.sda.absfactory;

public class MotorcycleNrPlate extends NrPlate {
    private Boolean lowEngineCapacity;

    public MotorcycleNrPlate(Language language, Size size, Boolean lowEngineCapacity) {
        super(language, size);
        this.lowEngineCapacity = lowEngineCapacity;
    }

    @Override
    public void show() {
        System.out.println(getNr() + " " + getSize() + " Engine Capacity: " + lowEngineCapacity);
    }

    public Boolean getLowEngineCapacity() {
        return lowEngineCapacity;
    }

    public void setLowEngineCapacity(Boolean lowEngineCapacity) {
        this.lowEngineCapacity = lowEngineCapacity;
    }
}
