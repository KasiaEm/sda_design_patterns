package com.sda.absfactory;

public class AmericanPlateFactory extends AbstractPlateFactory {
    @Override
    public NrPlate getPlate(PlateType plateType) {
        if(plateType.equals(PlateType.CAR)){
            return new CarNrPlate(Language.EN, Size.AMERICAN_CAR, true, false);
        } else if (plateType.equals(PlateType.MOTORCYCLE)){
            return new MotorcycleNrPlate(Language.EN, Size.AMERICAN_MOTORCYCLE, false);
        } else if (plateType.equals(PlateType.TRUCK)){
            return new TruckNrPlate(Language.EN, Size.AMERICAN_TRUCK, false);
        }
        return null;
    }
}
