package com.sda.absfactory;

import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PolishPlateFactory polishPlateFactory = new PolishPlateFactory();
        AmericanPlateFactory americanPlateFactory = new AmericanPlateFactory();

        List<NrPlate> nrPlates = new LinkedList<>();

        nrPlates.add(polishPlateFactory.getPlate(PlateType.CAR));
        nrPlates.add(polishPlateFactory.getPlate(PlateType.MOTORCYCLE));
        nrPlates.add(polishPlateFactory.getPlate(PlateType.TRUCK));
        nrPlates.add(americanPlateFactory.getPlate(PlateType.CAR));
        nrPlates.add(americanPlateFactory.getPlate(PlateType.MOTORCYCLE));
        nrPlates.add(americanPlateFactory.getPlate(PlateType.TRUCK));

        nrPlates.forEach(p -> p.show());
    }
}
