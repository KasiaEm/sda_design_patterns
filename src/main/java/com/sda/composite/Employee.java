package com.sda.composite;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

public class Employee {
    private String firstName;
    private String lastName;
    private Deparment deparment;
    private BigDecimal salary;
    private Set<Employee> subordinates = new HashSet<>();

    public Employee(String firstName, String lastName, Deparment deparment, BigDecimal salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.deparment = deparment;
        this.salary = salary;
    }

    public void addSubordinate(Employee employee){
        subordinates.add(employee);
    }

    public BigDecimal countSubordinatesSalaries(){
        return subordinates.stream().map(Employee::getSalary).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public void printEmployees(){
        printEmployees(1);
    }

    private void printEmployees(int level){
        for (int i = 0; i < level; i++) {
            System.out.print("-");
        }
        System.out.println(firstName + " " + lastName);
        for(Employee s: subordinates){
            s.printEmployees(level+1);
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Deparment getDeparment() {
        return deparment;
    }

    public void setDeparment(Deparment deparment) {
        this.deparment = deparment;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Set<Employee> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(Set<Employee> subordinates) {
        this.subordinates = subordinates;
    }
}
