package com.sda.composite;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Employee jdoe = new Employee("John", "Doe", Deparment.MANAGEMENT, BigDecimal.valueOf(10000));
        Employee msmith = new Employee("Mary", "Smith", Deparment.IT, BigDecimal.valueOf(5000));
        jdoe.addSubordinate(msmith);
        Employee pblack = new Employee("Preston", "Black", Deparment.IT, BigDecimal.valueOf(5000));
        jdoe.addSubordinate(pblack);
        Employee rkowalski = new Employee("Rod", "Kowalski", Deparment.IT, BigDecimal.valueOf(3000));
        pblack.addSubordinate(rkowalski);

        System.out.println("John's Doe subordinates salaries: " + jdoe.countSubordinatesSalaries());

        jdoe.printEmployees();

    }
}
